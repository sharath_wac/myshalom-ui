import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

@Component({
  selector: 'app-member-lists',
  templateUrl: './member-lists.component.html',
  encapsulation: ViewEncapsulation.None,  
})
export class MemberListsComponent implements OnInit {

  constructor(private _script: ScriptLoaderService) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this._script.loadScripts('app-member-lists',
      ['assets/demo/default/custom/crud/metronic-datatable/base/member-rendering.js']);
  }

}
