import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

@Component({
  selector: 'app-user-lists',
  templateUrl: './user-lists.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class UserListsComponent implements OnInit, AfterViewInit {

  constructor(private _script: ScriptLoaderService) { 

  }
  ngOnInit() {
  
  }
  ngAfterViewInit() {
    this._script.loadScripts('app-user-lists',
      ['assets/demo/default/custom/crud/metronic-datatable/base/column-rendering.js']);
  }

}
